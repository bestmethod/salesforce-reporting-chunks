.. salesforce-reporting-chunked documentation master file, created by
   sphinx-quickstart on Wed May 29 08:36:41 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

salesforce-reporting-chunked
============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: salesforce_reporting_chunked.chunky
    :members:

.. automodule:: salesforce_reporting_chunked.report_chunker
    :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
